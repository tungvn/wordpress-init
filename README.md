# Wordpress Starter Theme

## CI is added

## Workflow

### Clone based project
```
git mkdir project-name
git clone https://gitlab.com/timeuniversal/wordpress-base.git ./project-name
```

### Setup configs
* Duplicate `wp-config-sample.php` then rename  to `wp-config.php` and fill your DB information then access the website from your browser.

### Coding

#### Directories & Files
* Updating...

#### Included libraries
* Bootstrap: You can generate your project's bootstrap [here](getbootstrap.com/customize/) and then replace it in `src/static/bootstrap`.
* Font Awesome: List icon is [here](https://fortawesome.github.io/Font-Awesome/icons/)

## Contributors
* **Vũ Ngọc Tùng**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Thanks!


