<?php

/**
 * Define constants
 * These contants will be used globally
 */
define( 'BLOGNAME', get_option('blogname') );
define( 'HOME_URL', home_url('/') );
define( 'TEMPLATE_URL', get_template_directory_uri() );
define( 'ADMIN_AJAX_URL', admin_url( 'admin-ajax.php' ) );
define( 'IMAGE_URL', TEMPLATE_URL . '/assets/images' );
define( 'NO_IMAGE_URL', 'http://lorempixel.com/g/500/500/' );

/**
 * Define constants configs
 */
define( 'DISABLE_COMMENTS', true );
define( 'DISABLE_RSS', true );
define( 'SHOW_ADMIN_BAR', true );

/**
 * Including core stuff
 * Done remove those lines, your website can not start
 */
include_once( get_template_directory() . '/includes/init.php' );
include_once( get_template_directory() . '/includes/helpers.php' );
include_once( get_template_directory() . '/includes/wp-helpers.php' );

/**
 * Including post-types files
 */
//include_once( get_template_directory() . '/post-types/gallery.php' );

/**
 * Adding scripts and styles
 * All your scripts and styles will be included in wp_head()
 */
 add_action('wp_enqueue_scripts', 'tvn_enqueue_scripts_styles');
 function tvn_enqueue_scripts_styles() {
     if ( !wp_script_is('media') ) {
         wp_enqueue_media();
     }
 
     wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
     wp_enqueue_style( 'style', get_stylesheet_uri() );
 
     wp_enqueue_script('jquery');
     wp_enqueue_script('addons', get_template_directory_uri() . '/assets/scripts/addons.js', array(), '0.1', false);
     wp_enqueue_script('helpers', get_template_directory_uri() . '/assets/scripts/helpers.js', array(), '0.1', false);
     wp_enqueue_script('scripts', get_template_directory_uri() . '/assets/scripts/main.js', array(), '0.1', false);
 
     $wp_script_data = array(
         'ajaxurl' => ADMIN_AJAX_URL,
         'homeurl' => HOME_URL,
     );
 
     wp_localize_script( 'scripts', 'wp_vars', $wp_script_data );
 }
 
 /**
  * Adding scripts and styles to admin panel
  * All your scripts and styles will be included in wp_head()
  */
 add_action('admin_enqueue_scripts', 'tvn_admin_enqueue_scripts_styles');
 function tvn_admin_enqueue_scripts_styles() {
     wp_enqueue_media();
 
     wp_enqueue_script('jquery-ui-js', TEMPLATE_URL.'/assets/jquery-ui/jquery-ui.min.js');
     wp_enqueue_style('jquery-ui-style', TEMPLATE_URL.'/assets/jquery-ui/jquery-ui.min.css');
 
     $wp_script_data = array(
         'ajaxurl' => ADMIN_AJAX_URL,
         'homeurl' => HOME_URL,
     );
 
     wp_localize_script( 'scripts', 'wp_vars', $wp_script_data );
 }
 