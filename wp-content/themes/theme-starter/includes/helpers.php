<?php

/**
 * Remove space
 * @param $string
 * @return mixed
 */
function tvn_trim_space( $string ) {
    $string = preg_replace( '/\s+/', ' ', $string );
    return $string;
}

/**
 * Convert Hex to RGB
 * @param $hex
 * @return array
 */
function tvn_hex_to_rgb( $hex ) {
    $hex = str_replace("#", "", $hex);

    if( strlen($hex) == 3 ) {
        $r = hexdec( substr($hex, 0, 1) . substr($hex, 0, 1) );
        $g = hexdec( substr($hex, 1, 1) . substr($hex, 1, 1) );
        $b = hexdec( substr($hex, 2, 1) . substr($hex, 2, 1) );
    } else {
        $r = hexdec( substr($hex, 0, 2) );
        $g = hexdec( substr($hex, 2, 2) );
        $b = hexdec( substr($hex, 4, 2) );
    }
    $rgb = array( $r, $g, $b );

    return $rgb;
}

/**
 * Encrypt - Two way encoding
 * @param $input
 * @param $key_seed
 * @return string
 */
function tvn_encode( $input, $key_seed ) {
    $input = trim($input);
    $block = mcrypt_get_block_size('tripledes', 'ecb');
    $len = strlen($input);
    $padding = $block - ($len % $block);
    $input .= str_repeat(chr($padding), $padding);
    $key = substr(md5($key_seed), 0, 24);
    $iv_size = mcrypt_get_iv_size(MCRYPT_TRIPLEDES, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $encrypted_data = mcrypt_encrypt(MCRYPT_TRIPLEDES, $key, $input, MCRYPT_MODE_ECB, $iv);
    return base64_encode($encrypted_data);
}

/**
 * Decrypt - Two way encoding
 * @param $input
 * @param $key_seed
 * @return string
 */
function tvn_decode( $input, $key_seed ) {
    $input = base64_decode($input);
    $key = substr(md5($key_seed), 0, 24);
    $text = mcrypt_decrypt(MCRYPT_TRIPLEDES, $key, $input, MCRYPT_MODE_ECB, '12345678');
    $block = mcrypt_get_block_size('tripledes', 'ecb');
    $packing = ord($text{strlen($text) - 1});
    if ($packing and ($packing < $block)) {
        for ($P = strlen($text) - 1; $P >= strlen($text) - $packing; $P--) {
            if (ord($text{$P}) != $packing) {
                $packing = 0;
            }
        }
    }
    $text = substr($text, 0, strlen($text) - $packing);
    return $text;
}

/**
 * Create zip file
 * @param array $files
 * @param string $destination
 * @param bool $overwrite
 * @param string $contain_folder
 * @return bool
 */
function tvn_create_zip( $files = array(), $destination = '', $overwrite = false, $contain_folder = '' ) {
    $exist = file_exists($destination);
    if ($exist && !$overwrite) {
        return $exist;
    }

    $valid_files = array();
    if (is_array($files)) {
        foreach ($files as $file) {
            if (file_exists($file)) {
                $valid_files[] = $file;
            }
        }
    }
    if (count($valid_files)) {
        $zip = new ZipArchive();
        if ($zip->open($destination, $overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
            return false;
        }

        if($contain_folder) $contain_folder = $contain_folder.'/';
        foreach ($valid_files as $file) {
            $zip->addFile($file, $contain_folder.aj_get_file_name($file, true));
        }
        $zip->close();

        return file_exists($destination);
    } else {
        return false;
    }
}
