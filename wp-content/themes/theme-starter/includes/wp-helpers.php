<?php 

/**
 * Get permalink by path and post type
 * @param $path <string> Path of post type needed
 * @param $post_type <string> Post type
 * @return <string>
 */
function get_permalink_by_path( $path, $post_type = 'page' ) {
    ${$post_type} = get_page_by_path( $path, OBJECT_K, $post_type );
    if( false === ${$post_type} ) {
        return '';
    }

    return get_permalink( ${$post_type} );
}

/**
 * Get permalink by id and size
 * @param $path <string> Path of post type needed
 * @param $post_type <string> Post type
 * @return <string>
 */
function wp_get_image_src( $id, $size = 'medium' ) {
    $id = intval( $_POST['id'] );

	if( 0 < $id && 'attachment' == get_post_type( $id ) ) {
		$img = wp_get_attachment_image_src( $id, $size );
        return $img[0];
    }

    return 0;
}