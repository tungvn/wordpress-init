<?php

/**
 * Setup site menus
 */
register_nav_menus(array(
    'main-menu' => 'Main Menu'
));

/**
 * Image sizing
 * You can add more size if you need
 */
if( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );

    //Default: 150x150, 300x300, 1024x1024
}

/**
 * Setup text domain
 * Just use "timevn", no need to change this
 * The language files will be stored in "languages" folder in your root
 */
add_action( 'init', 'setup_theme_textdomain' );
function setup_theme_textdomain() {
    load_theme_textdomain( 'tvn', get_template_directory() . '/languages' );
}

/**
 * Custom admin login header url
 */
add_filter( 'login_headerurl', 'tvn_login_custom_link' );
function tvn_login_custom_link() {
    return HOME_URL;
}

/**
 * Custom admin login header title
 */
add_filter( 'login_headertitle', 'tvn_change_title_on_logo' );
function tvn_change_title_on_logo() {
    return BLOGNAME;
}

/**
 * Cutome admin logo
 */
add_action( 'login_enqueue_scripts', 'tvn_change_wp_admin_logo' );
function tvn_change_wp_admin_logo() { ?>
    <style type="text/css">
        body.login{}
        body.login div#login{padding: 1% 0px 0px;}
        body.login div#login h1 a{background: url("<?php echo IMAGE_URL .'/logo.png'; ?>") no-repeat center center / 100% 100%; width: 90px; height: 66px;}
    </style>
<?php }

/**
 * Remove admin bar - Optional
 */
add_filter( 'show_admin_bar', '__return_false' );

/**
 * Security
 */
remove_action( 'wp_head', 'wp_generator') ; 
remove_action( 'wp_head', 'wlwmanifest_link' );

if( DISABLE_RSS ) {
    remove_action( 'wp_head', 'rsd_link' );
    remove_action( 'wp_head', 'feed_links', 2 );
    remove_action( 'wp_head', 'feed_links_extra', 3 );
}

/**
 * Remove comment icon on admin bar
 */
add_action( 'wp_before_admin_bar_render', 'tvn_remove_edit_comments_admin_bar' );
function tvn_remove_edit_comments_admin_bar() {
    if( DISABLE_COMMENTS ) {
        global $wp_admin_bar;
        $wp_admin_bar->remove_menu( 'comments' );
    }
}

/**
 * Remove admin comment page
 */
add_action( 'admin_menu', 'tu_remove_admin_comments_page' );
function tu_remove_admin_comments_page(){
    if( DISABLE_COMMENTS ) {
        remove_menu_page( 'edit-comments.php' );
    }
}

/**
 * Remove admin comment support
 */
add_action( 'init', 'tu_remove_comment_support', 100 );
function tu_remove_comment_support() {
    if( DISABLE_COMMENTS ) {
        remove_post_type_support( 'post', 'comments' );
        remove_post_type_support( 'page', 'comments' );
        remove_post_type_support( 'attachment', 'comments' );
    }
}

/**
 * Remove accents from file name when uploading
 */
add_filter('sanitize_file_name', 'sanitize_file_name_remove_accents', 10);
function sanitize_file_name_remove_accents( $filename ) {
	return remove_accents( $filename );
}

/**
 * Custom excerpt's length
 */
// add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
function custom_excerpt_length( $length ) {
    return 50;
}

/**
 * Custom excerpt's more
 */
// add_filter('excerpt_more', 'new_excerpt_more');
function new_excerpt_more( $more ) {
    return ' ...';
}
