<?php

/**
 * INITIALIZE POST TYPE
 */

/**
 * Register post type
 */
add_action('init', 'reg_post_type_contact');
function reg_post_type_contact()
{
    $labels = array(
        'name' => __('Liên hệ', 'tu'),
        'singular_name' => __('Liên hệ', 'tu'),
        'menu_name' => __('Liên hệ', 'tu'),
        'all_items' => __('Tất cả liên hệ', 'tu'),
        'add_new' => __('Thêm mới', 'tu'),
        'add_new_item' => __('Thêm mới liên hệ', 'tu'),
        'edit_item' => __('Chỉnh sửa liên hệ', 'tu'),
        'new_item' => __('Liên hệ mới', 'tu'),
        'view_item' => __('Xem chi tiết', 'tu'),
        'search_items' => __('Tìm kiếm', 'tu'),
        'not_found' => __('Không tìm thấy bản ghi nào', 'tu'),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', 'tu'),
        'view' => __('Xem liên hệ', 'tu')
    );

    $args = array(
        'labels' => $labels,
        'description' => __('Liên hệ', 'tu'),
        'public' => false,
        'publicly_queryable' => false,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        'rewrite' => null,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-feedback',
        'supports' => array('title'),
        'has_archive' => false
    );

    register_post_type('contact', $args);
}

/**
 * ADDING META BOXES
 */

add_action('admin_init', 'add_metabox_contact');
function add_metabox_contact()
{
    /**
     * Meta box for general information
     * @param $post
     */
    function display_metabox_contact_general($post)
    {
        $post_id = $post->ID;
        $contact_email = get_post_meta($post_id, 'contact_email', true);
        $contact_fullname = get_post_meta($post_id, 'contact_fullname', true);
        $contact_phone = get_post_meta($post_id, 'contact_phone', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_metabox_contact'); ?>">
            <tbody>
            <tr>
                <th scope="row"><label for="">Họ tên</label></th>
                <td><?php echo $contact_fullname; ?></td>
            </tr>
            <tr>
                <th scope="row"><label for="">Email</label></th>
                <td><?php echo $contact_email; ?></td>
            </tr>
            <tr>
                <th scope="row"><label for="">SĐT</label></th>
                <td><?php echo $contact_phone; ?></td>
            </tr>
            </tbody>
        </table>
    <?php
    }

    add_meta_box(
        'display_metabox_contact_general',
        'Thông tin cơ bản',
        'display_metabox_contact_general',
        'contact',
        'normal',
        'high'
    );
}